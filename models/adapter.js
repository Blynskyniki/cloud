const mongo = require('mongoose');
require('dotenv').config();

mongo.connect(`mongodb://${process.env.MYDBADDR}:${process.env.MYDBPORT}`, {
  useMongoClient: true,
});

mongo.Promise = global.Promise;
module.exports = mongo;
