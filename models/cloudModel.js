/* eslint-disable comma-dangle */
const mongo = require('./adapter');
const findOrCreate = require('mongoose-findorcreate');

const recordShema = mongo.Schema({
  any: {
    type: mongo.Schema.Types.Mixed,
    unique: true,
    default: {},
  },
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updatedAt',
  },

});

recordShema.plugin(findOrCreate);
module.exports = mongo.model('cloud', recordShema);

