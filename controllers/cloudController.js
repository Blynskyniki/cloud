/* eslint-disable guard-for-in,no-restricted-syntax,prefer-const,no-undef */
const log = require('winston');
const cloud = require('../models/cloudModel');

require('dotenv').config();

log.level = process.env.LOGLEVEL;
const crud = {};

// --------------------Создание устройства
crud.create = async (req, reply) => {
  try {
    if (!crud.preValidation(req.payload.data)) {
      log.debug('No valid JSON request');
      reply({
        error: 'No valid JSON request',
      }).code(503);
    }
    const query = [];
    for (record in req.payload.data) {
      query[record] = crud.generateShema(req.payload.data[record], req.params.elemType);
    }
    const elements = await cloud.create(query);
    log.debug('Result for create :', JSON.stringify(elements, '', '   '));
    reply(elements).code(200);
  } catch (err) {
    log.error(err);
    reply({
      error: err,
    }).code(500);
  }
};


crud.list = async (req, reply) => {
  try {
    let anyArray = [];
    const elements = await cloud.find({
      'any.elemType': {$regex: req.params.elemType, $options: 'i'},
    }).select('any');
    elements.map((element) => {
      anyArray.push(element.any);
    });
    log.debug('Find :', JSON.stringify(elements, '', '   '));
    reply(anyArray).code(200);
  } catch (err) {
    log.error(err);
    reply({
      error: err,
    }).code(500);
  }
};

crud.listForFieldType = async (req, reply) => {
  log.debug('Find query');
  try {
    const filter = {
      'any.elemType': req.params.elemType.toString(),
    };
    filter[`any.${req.params.fieldType1.toString()}`] = {$exists: true};
    filter[`any.${req.params.fieldType2.toString()}`] = {$exists: true};
    const elements = await cloud.find(filter);
    log.debug('Find :', JSON.stringify(elements, '', '   '));

    if (elements.length < 1) {
      reply({}).code(200);
      return;
    }
    reply(elements).code(200);
  } catch (err) {
    log.error(err);
    reply({
      error: err,
    }).code(500);
  }
};
crud.listFor = async (req, reply) => {
  try {
    let anyArray = [];
    const filter = {
      'any.elemType': req.params.elemType,
    };
    filter[`any.${req.params.fieldType}`] = req.params.fieldValue.toString();


    const elements = await cloud.find(filter);
    log.debug('Find :', JSON.stringify(elements, '', '   '));
    elements.map((element) => {
      anyArray.push(element.any);
    });
    if (elements.length < 1) {
      reply({}).code(200);
      return;
    }
    reply(anyArray).code(0);
  } catch (err) {
    log.error(err);
    reply({
      error: err,
    }).code(500);
  }
};


crud.delete = async (req, reply) => {
  if (!crud.preValidation(req.payload.data)) {
    log.debug('No valid JSON request');
    reply({
      error: 'No valid JSON request',
    }).code(503);
    return;
  }

  try {
    const allRecords = req.payload.data;
    const query = [];
    for (record in allRecords) {
      const filter = {
        'any.elemType': req.params.elemType,
      };
      const thisRecord = allRecords[record];
      for (key in thisRecord) {
        filter[`any.${key}`] = thisRecord[key].toString();
      }
      query[record] = filter;
    }
    const elements = await cloud.remove({$or: query});
    log.debug('Delete :', JSON.stringify(elements, '', '   '));

    reply({
      quantity: elements.result.n,
    }).code(0);
  } catch (err) {
    log.error(err);
    reply({
      error: err,
    }).code(500);
  }
};
crud.deleteForElemtype = async (req, reply) => {
  try {
    const filter = {
      'any.elemType': req.params.elemType,
    };


    const elements = await cloud.remove(filter);
    log.debug('Delete :', JSON.stringify(elements, '', '   '));

    reply({
      quantity: elements.result.n,
    }).code(0);
  } catch (err) {
    log.error(err);
    reply({
      error: err,
    }).code(500);
  }
};

crud.preValidation = (payload) => {
  if (typeof (payload) === 'object') {
    for (record in payload) {
      if (Object.entries(payload[record]).length < 2) {
        return false;
      }
    }
    return true;
  }
  return false;
};

crud.isExistRecord = async (record, elemType) => {
  const filter = {
    'any.elemType': {
      $regex: elemType,
      $options: 'i',
    },
  };


  for (key in record) {
    filter[`any.${key}`] = record[key].toString();
  }

  const element = await cloud.findOne(filter);

  if (element) {
    return true;
  }
  return false;
};


crud.generateShema = (record, elemType) => {
  const fields = {};
  fields.any = record;
  fields.any.elemType = elemType;
  // log.debug('Generate conditions for record  :', JSON.stringify(fields, '', '   '));
  return fields;
};

module.exports = crud;
