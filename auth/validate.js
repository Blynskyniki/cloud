/* eslint-disable global-require,no-plusplus,no-undef,key-spacing,no-use-before-define,guard-for-in,no-restricted-syntax,keyword-spacing,no-shadow,semi-spacing */
const JwT = require('jsonwebtoken');
require('dotenv').config();

exports.register = function(plugin, options, next) {
  plugin.register({ register: require('hapi-auth-bearer-token') }, (err) => {
    plugin.auth.strategy('bearer', 'bearer-access-token', {
      validateFunc: (token, cb) => {
        try {
          const tokenCount = process.env.TOKENCOUNT;
          let auth = false;
          console.log('VALIDATE START');
          console.log('COUNT ATTEMPS keys', tokenCount);
          for (itemToken = 1; itemToken <= tokenCount; itemToken++) {
            const tokenName = `TOKEN${itemToken}`;
            const key = process.env[tokenName];
            console.log('Key', {
              tname: tokenName,
              number: itemToken,
              k: key,
              t: token,
            });
            if (validate(token, key)) {

              auth = true;
              break;
            }
          }
          if (auth) {
            console.log('VALID +++++++');
            return cb(null, true, { token });
          }
          return cb(null, false, { token });
        } catch (err) {
          console.log('Validate ', err);
          return cb(null, false, { token });
        }
      },
    });

    next();
  });
};

function validate(t, k) {
  try {
    JwT.verify(t, k);
    return true;
  } catch (e) {
    console.log('NO VALID');
    return false;
  }
}


exports.register.attributes = {
  pkg: {
    name: 'authentication',
    description: 'This is the authentication provider',
    main: 'auth.js',
    author: 'John Brett',
    license: 'MIT',
  },
};
