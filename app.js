/* eslint-disable global-require,no-trailing-spaces */
const Hapi = require('hapi');
const log = require('winston');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const auth = require('./auth/validate');
require('dotenv').config();


// log.level = process.env.LOGLEVEL;


module.exports = async (cb) => {
  try {
    const server = new Hapi.Server();
    server.connection({
      port: process.env.PORT,
      routes: { cors: true },
    });
    await server.register([
      Inert,
      Vision,

      {
        register: HapiSwagger,
        options: {
          info: {
            title: `${Pack.name} API Documentation`,
            version: Pack.version,
          },
          securityDefinitions: {
            Bearer: {
              type: 'apiKey',
              name: 'Authorization',
              //  value: 'eyJhbGciOiJIUzI1NiJ9.MTIzNA.1NhiLENAcUn23mGDb-st7RK3YZPiQSSD8vXq-oXn6BU',
              in: 'header',
            },
          },
          security: [{Bearer: []}],
        },
      },
      auth,

    ]);


    // server.auth.default('bearer');
    server.route(require('./routes/routes'));
    cb(null, server);
  } catch (err) {
    log.error(err);
    cb(err, null);
  }
};
