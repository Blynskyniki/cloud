#!/usr/bin/env bash
!#/bin/bash

echo "Создание контейнера"
docker build -t blynskyniki/save_records_in_cloud:$1 ./
echo "Тегирование контейнера "
docker tag blynskyniki/save_records_in_cloud:$1  blynskyniki/save_records_in_cloud:$1
echo "Заливка  контейнера"
docker push blynskyniki/save_records_in_cloud:$1
echo "Готово! контейнер : blynskyniki/save_records_in_cloud:$1"