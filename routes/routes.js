/* eslint-disable import/no-extraneous-dependencies */

const cloudController = require('./../controllers/cloudController');
const Joi = require('joi');

const responseCodes = {
  'hapi-swagger': {
    responses: {
      200: {description: 'Выполнено'},
      404: {description: 'Не найдено'},
      503: {description: 'Что-то сломалось :-('},
      401: {description: 'Не авторизован'},
      400: {description: 'Неправильный токен'},
      409: {description: 'Конфликт данных'},
    },
  },
};

const routes = [

  // -------------- Новая запись
  {
    method: 'POST',
    path: '/create/{elemType}',
    handler: cloudController.create,
    config: {
      description: 'Создание новой записи',
      auth: {
        strategy: 'bearer',
      },
      tags: ['api'],
      plugins: responseCodes,
      validate: {
        params: {
          elemType: Joi.string().required().description('Например : my1cEvotorCloud'),
        },
        payload: {
          data: Joi.array().required().example([{
            '1c': '416464',
            evotor: '123',
          }, {
            '1c': '8798797',
            evotor: '119494',

          }]),
        },
      },
    },
  },
  // -------------- Список всего запись
  {
    method: 'GET',
    path: '/get/all/{elemType}',
    handler: cloudController.list,
    config: {
      description: 'Получить все записи по ElemType',
      auth: {
        strategy: 'bearer',
      },
      tags: ['api'],
      plugins: responseCodes,
      validate: {
        params: {
          elemType: Joi.string().required().description('Например : my1cEvotorCloud'),
        },

      },
    },
  },
  // -------------- Список всего по elemtype и fieldtyp'am
  {
    method: 'GET',
    path: '/get/all/{elemType}/{fieldType1}/{fieldType2}',
    handler: cloudController.listForFieldType,
    config: {
      description: 'Получить все записи по elemType и двум fieldType ',
      auth: {
        strategy: 'bearer',
      },
      tags: ['api'],
      plugins: responseCodes,
      validate: {
        params: {
          elemType: Joi.string().required().description('Например : my1cEvotorCloud'),
          fieldType1: Joi.string().required().description('Например : 1с'),
          fieldType2: Joi.string().required().description('Например : evotor'),
        },

      },
    },
  },
  // -------------- Поиск  всего по elemtypee
  {
    method: 'GET',
    path: '/get/{elemType}/{fieldType}/{fieldValue}',
    handler: cloudController.listFor,
    config: {
      description: 'Получить  запись по elemType, fieldType, fieldValue  ',
      auth: {
        strategy: 'bearer',
      },
      tags: ['api'],
      plugins: responseCodes,
      validate: {
        params: {
          elemType: Joi.string().required().description('Например : my1cEvotorCloud'),
          fieldType: Joi.string().required().description('Например : 1c'),
          fieldValue: Joi.string().required().description('Например : 416464'),
        },


      },
    },
  },
  // -------------- Удаление записи
  {
    method: 'POST',
    path: '/delete/{elemType}',
    handler: cloudController.delete,
    config: {
      description: 'Удаление записей ',
      auth: {
        strategy: 'bearer',
      },
      tags: ['api'],
      plugins: responseCodes,
      validate: {
        params: {
          elemType: Joi.string().required().description('Например : my1cEvotorCloud'),
        },
        payload: {
          data: Joi.array().required().example([{
            '1c': '416464',
            evotor: '123',
          }, {
            '1c': '8798797',
            evotor: '119494',

          }]),
        },

      },
    },
  },
  // -------------- Удаление записей по elemtype
  {
    method: 'POST',
    path: '/delete/all/{elemType}',
    handler: cloudController.deleteForElemtype,
    config: {
      description: 'Удаление всех записей по Elemtype ',
      auth: {
        strategy: 'bearer',
      },
      tags: ['api'],
      plugins: responseCodes,
      validate: {
        params: {
          elemType: Joi.string().required().description('Например : my1cEvotorCloud'),
        },
      },
    },
  },
];
module.exports = routes;
