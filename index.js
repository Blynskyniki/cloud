/* eslint-disable no-shadow */
const Server = require('./app');
const log = require('winston');
require('dotenv').config();

log.level = process.env.LOGLEVEL;

Server(async (err, server) => {
  if (err) {
    throw err;
  }
  try {
    await server.start();
    log.info(`Server running at: ${server.info.uri}`);
  } catch (err) {
    log.error(err);
  }
});
